var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var mongoDB = 'mongodb+srv://suleman8046:yuUC2k447MbpAjZ@first-cluster-talha.dwjos.mongodb.net/node-js-the-net-ninja?retryWrites=true&w=majority';

mongoose.connect(mongoDB, { useNewUrlParser: true });
var todoSchema = new mongoose.Schema({
    item: String
});
var Todo = mongoose.model('Todo', todoSchema);
var itemOne = Todo({item: 'buy flower'}).save(function(err){
    if(err) throw err;
    console.log('item saved');
});


var data = [{item: 'get milk'}, {item: 'walk dog'}, {item: 'kick some coding ass'}];
var urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function(app){

app.get('/todo', function(req, res){
    res.render('todo', {todos: data});
});

app.post('/todo', urlencodedParser, function(req, res){
    data.push(req.body);
    res.json(data);
});

app.delete('/todo/:item', function(req, res){
    data = data.filter(function(todo){
        return todo.item.replace(/ /g, '-') != req.params.item;
    });
    res.json(data);
});

};